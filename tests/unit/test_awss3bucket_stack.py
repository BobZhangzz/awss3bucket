import aws_cdk as core
import aws_cdk.assertions as assertions

from awss3bucket.awss3bucket_stack import Awss3BucketStack

# example tests. To run these tests, uncomment this file along with the example
# resource in awss3bucket/awss3bucket_stack.py
def test_sqs_queue_created():
    app = core.App()
    stack = Awss3BucketStack(app, "awss3bucket")
    template = assertions.Template.from_stack(stack)

#     template.has_resource_properties("AWS::SQS::Queue", {
#         "VisibilityTimeout": 300
#     })
