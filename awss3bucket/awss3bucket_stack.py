from aws_cdk import (
    core,
    aws_s3 as s3
)

class Awss3BucketStack(core.Stack):

    def __init__(self, scope: core.Construct, id: str, **kwargs) -> None:
        super().__init__(scope, id, **kwargs)

        # Define an S3 bucket
        bucket = s3.Bucket(self, "MyBucket",
            versioned=True,  # Enable versioning
            encryption=s3.BucketEncryption.S3_MANAGED  # Use S3-managed encryption
        )
